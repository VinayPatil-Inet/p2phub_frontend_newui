
import { Container, Row, Col, Form, Button, Card, CloseButton } from "react-bootstrap";
import React, { Component, useState, useEffect } from 'react';
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faSearch, faPlus, faEye, faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { Link, useHistory, useParams } from 'react-router-dom';
import organization_img from './massachusetts.png'
import { CircularProgress, useScrollTrigger } from "@material-ui/core";
import paginationFactory from 'react-bootstrap-table2-paginator';
import Table from 'react-bootstrap/Table'
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';



const ViewOrganization =() =>{
  const { value } = useParams();
  const [userList, setUserList] = useState([]);
  const [payerList, setPayerList] = useState([]);

  const [endpointNamelist,setEndPointListNames]=useState([]);
  const [endpointAdminNamelist,setEndPointAdminListNames]=useState([]);

  const [errorMessage, setErrorMessage] = React.useState("");
  const [successMessage, setSuccessMessage] = React.useState("");


  var isadmin = sessionStorage.getItem('isadmin');
  var is_approve = sessionStorage.getItem('is_approve');
   console.log(is_approve,"isapprove")

  const columns = [
    { dataField: 'payer_id', text: 'Id' },
   { dataField: 'name', text: 'Name' },
   { dataField: 'email', text: 'Email' },
   { dataField: 'payer_url', text: 'Payer Url' },
   {
    dataField: "is_approve",
    text: "Action",
    formatter: (cellContent, row) => {

        return (

          <div className="w-100 text-center mt-2">

          <button

              className="btn btn-success btn-xs" Width="95px"
              style={{ margin: "2px" }}
              onClick={() => updateApproveById(row.payer_id)}
          >
              Approve
          </button>


          {/* <Link to={`/viewpayer/${row.payer_id}`}> */}
                  <button
                      className="btn btn-danger btn-xs" Width="95px"
                      style={{ margin: "2px" }}
                     onClick={() => updateRejectedById(row.payer_id)}
                  >
                     Rejected
                  </button>

              {/* </Link> */}
              </div>


        );
    },
},
]

  const pagination = paginationFactory({
    page: 1,
    sizePerPage: 5,
    lastPageText: '>>',
    firstPageText: '<<',
    nextPageText: '>',
    prePageText: '<',
    showTotal: true,
    alwaysShowAllBtns: true,
    onPageChange: function (page, sizePerPage) {
        console.log('page', page)
        console.log('sizePerPage', sizePerPage)
    },
    onSizePerPageChange: function (page, sizePerPage) {
        console.log('page', page)
        console.log('sizePerPage', sizePerPage)
    }
})




  useEffect(() => {
   // getMyEndPointsList()
   getMyEndPointsList_payer_id()
    getAdminMyEndPointsList()
    var user_id = sessionStorage.getItem('userid');
        console.log(user_id,"user_id")


    axios
      .get(process.env.REACT_APP_BASE_URL+`/api/organization/getOrganisationByViewId/${value}`)
      .then((res) => {

        setUserList(res.data.data);
        console.log(res.data.data,"getOrganisationByViewId")
        console.log("payerid ", sessionStorage.getItem('payerid'));
      });
  },
  []);

  var payer_id = sessionStorage.getItem('payerid');
  console.log(payer_id,"payer_id")
function getMyEndPointsList() {
              axios.get(process.env.REACT_APP_BASE_URL+`/api/getEndPointsAll/${user_id}`).then(res => {
                  setEndPointListNames(res.data.data);
                  console.log(res.data.data,"getMyEndPointsList")
             });


 }
 //var value = sessionStorage.getItem('payerid');
 //console.log(value,"value")
function getMyEndPointsList_payer_id() {
             axios.get(process.env.REACT_APP_BASE_URL+`/api/getAllEndPoints/${value}`).then(res => {
                 setEndPointListNames(res.data.data);
                 console.log(res.data.data,"getAllEndPoints")
            });


}
 var user_id = sessionStorage.getItem('userid');
 console.log(user_id,"user_id")
 function getAdminMyEndPointsList() {
    axios.get(process.env.REACT_APP_BASE_URL+`/api/getAdminEndPointsAll/${user_id}`).then(res => {
        setEndPointAdminListNames(res.data.data);
        console.log(res.data.data,"getAdminMyEndPointsList")
   });


}
function updateApproveById(value) {
    console.log(value, "updateApproveById")
     axios.put(process.env.REACT_APP_BASE_URL+`/api/updateIsApprove/${value}`)
    //     setPayerList(res.data.data);
    //     console.log(res.data.data, "updateApprove");

        .then(response => {
          setSuccessMessage("Successfully Approved!")

          setTimeout(() => {
              setSuccessMessage()
          }, 4000);
          console.log(response.data, "documentationdetails")
      })
      .catch(error => {
          setErrorMessage("Not Approved")

          setTimeout(() => {
              setErrorMessage()
          }, 4000);
          console.log(error.data)
      });
}
function updateRejectedById(value) {
  console.log(value, "updateRejectedById")
  axios.put(process.env.REACT_APP_BASE_URL+`/api/updateIsRejected/${value}`)  .then(response => {
    setSuccessMessage("Successfully Reject!")

    setTimeout(() => {
        setSuccessMessage()
    }, 4000);

})
.catch(error => {
    setErrorMessage("Not Rejected")

    setTimeout(() => {
        setErrorMessage()
    }, 4000);
    console.log(error.data)
});

}
function getPayers() {

  getPayerList('All', 'All')
  var payerId = sessionStorage.getItem('payerid')
  getAddMyList(payerId)

}
function getPayerList(name, label) {
  console.log("getPayerList function involked ", name, label)
  var value = sessionStorage.getItem('payerid')

  axios.get(process.env.REACT_APP_BASE_URL+`/api/organization/get/${name}/${label}/${value}`).then(res => {
      setUserList(res.data.data);
      console.log(res.data.data, "getPayerList");


  });
}
function getAddMyList(value) {
  console.log(value, "getAddMyList")
  axios.get(process.env.REACT_APP_BASE_URL+`/api/getOrganisationAddMyList/${value}`).then(res => {
      setPayerList(res.data.data);
      console.log(res.data.data, "AAAAA");
  });
}
function createContract(destinatoin_payer_id) {

  var source_payer_id = sessionStorage.getItem('payerid');
  var user_id = sessionStorage.getItem('userid');

  var addArr = {
      'destinatoin_payer_id': destinatoin_payer_id,
      //'destinatoin_payer_id': 4,
      'source_payer_id': source_payer_id, 'user_id': user_id
  };
  console.log(addArr, "addArr")
  axios.post(
      process.env.REACT_APP_BASE_URL+'/api/PayersSignedStatus/create',
      addArr,

  )
      .then(response => {
          setSuccessMessage("Successfully created a payer!")
          console.log(response.data,"sucess");


         // getPayers();
      // console.log(response.body.data[0].payer_signed_status_id,"response.data.payer_signed_status_id")
         window.open(`http://74.235.67.55/devportal/login.php?id=${response.data.data[0].payer_signed_status_id}`, 'name', 'height=500,width=850')
          setTimeout(() => {
              setSuccessMessage()
          }, 2000);
          console.log(response.data.data[0].payer_signed_status_id,"payer_signed_status_id")
      })
      .catch(error => {
          console.log(error,"catch")
          setErrorMessage("Cannot created payer")
         // reset(error.data);
          setTimeout(() => {
              setErrorMessage()
          }, 2000);
          console.log(error.data)
      })
}

  return (
    <Container fluid="md">
    <Row>
        <Col md={12} style={{ textAlign: "left" }}>
            <Card style={{ width: "100%" }} >
                <Card.Body>


                    {/* <h3 className='mb-3 heading'>Blue Cross and Blue Shield of Massachusetts </h3> */}
                    <div class="pull-left">
                       {userList.map(x =>
                            <div className="w-100 text-center mt-2">
                                <h3 className='mb-3 heading'>{x.name}</h3>

                            </div>
                        )}
                    </div>
                    <h4> {successMessage && <div className="d-flex justify-content-center error" style={{ color: "green" }} > {successMessage}</div>} </h4>
                    <h4> {errorMessage && <div className="d-flex justify-content-center error" style={{ color: "red"}} > {errorMessage}</div>} </h4>

                   <div class="pull-right">
                       {/* {userList.map(x =>
                            <div className="w-100 text-center mt-2">
                            <button
                                className="btn btn-primary " Width="95px"
                                style={{ marginTop: "26px", borderRadius: "20px" }} id="download_button"
                                onClick={() => {

                                    createContract(x.value); window.open(`http://13.92.80.150/devportal/login.php?id=1`, 'name', 'height=500,width=850') }}
                            >
                                Add Organization
                            </button>
                        </div>
                        )}    */}

                                 {
            userList.map( x => {
              if( x.payer_status === 'Signed') {
                return   <div className="w-100 text-center mt-2">

            </div>
              }
              if(isadmin === 'true') {
                return   <div className="w-100 text-center mt-2">

            </div>
              }
             else {
                return <div className="w-100 text-center mt-2">
                <button
                    className="btn btn-primary " Width="95px"
                    style={{ marginTop: "26px", borderRadius: "20px" }} id="download_button"
                    onClick={() => {

                        createContract(x.value);  }}
                >
                    Add Organization
                </button>
            </div>

             }

            })
          }


                    </div>


                    <div class="clearfix"></div>

                    <div style={{ display: 'block', padding: 30 }}>


                        <Tabs defaultActiveKey="first">
                            <Tab eventKey="first" title="Organization Profile">
                                <img src={organization_img} alt="pic" /><br></br>
                                <Table className="view_tbl">

                                    {userList.map(user => {
                                        const { name, organisation_type, website, policylink, conditionlink, address1, city, state, phone } = user;
                                        return (
                                            <div >
                                                <tr>
                                                    <td><strong>Organization Name:</strong></td>
                                                    <td>{name}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Organization Type:</strong></td>
                                                    <td>{organisation_type}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Organization website:</strong></td>
                                                    <td>{website}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Organization Privacy Policy:</strong></td>
                                                    <td>{policylink}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Organization Terms and Conditions:</strong></td>
                                                    <td>{conditionlink}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Address:</strong></td>
                                                    <td>{address1},<br></br>
                                                        {city},<br></br>
                                                        {state}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Phone Number:</strong></td>
                                                    <td>{phone}</td>
                                                </tr>

                                            </div>
                                        )
                                    })
                                }

                                </Table>
                            </Tab>

                            <Tab eventKey="second" title="Endpoints">


                                {/* Hii, I am 2nd tab content */}
                                <Table className="view_tbl">

                                  <p><strong>Endpoint Name:</strong></p>
                                      {endpointNamelist.map(x => {
                                          const { endpoint_name } = x;
                                          return (
                                              <div >
                                                      <li>{endpoint_name}</li>
                                              </div>
                                          )
                                      })
                                      }
                                      <br></br>
                                       <p><strong>Base Url:</strong></p>

                                            {endpointNamelist.slice(0, 1).map(x => {
                                                const { base_url } = x;
                                                return (
                                                    <div >
                                                        <li>{base_url}</li>
                                                    </div>
                                                )
                                            })
                                            }
                                       <br></br>
                                       <p><strong>Auth Scope:</strong></p>
                                      {endpointNamelist.slice(0, 1).map(x => {
                                          const {auth_scope } = x;
                                          return (
                                              <div >
                                                      <li>{auth_scope}</li>
                                              </div>
                                          )
                                      })
                                      }
{/*
                            </div>
                          )

                      }
                    })()}  */}




                                </Table>
                            </Tab>
                        </Tabs>
          {
            userList.map( x => {
              if( x.is_approve == 0 && isadmin =='true' ) {
                return   <div className="w-100 text-center mt-2">
                 <Link to={`/AdminDashboard`}>
                 <button

                className="btn btn-success btn-xm" Width="95px"
                style={{margin:"2px"}}

                onClick={() => updateApproveById(x.value)}
            >
                Approve
            </button>
            </Link>
            <Link to={`/AdminDashboard`}>
            <button
            className="btn btn-danger btn-xm" Width="95px"
            style={{margin:"2px"}}

            onClick={() => updateRejectedById(x.value)}
        >
            Reject
        </button>
        </Link>
        </div>
              }
            })
          }
                    </div>


                </Card.Body>

            </Card >
        </Col>
    </Row>

</Container >



  );

}

export default ViewOrganization





