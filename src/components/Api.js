import axios from 'axios';

//const usersUrl = 'http://localhost:7000/users';


export const getUsers = async (id) => {
    id = id || '';
    return await axios.get(process.env.REACT_APP_BASE_URL+`/api/getIdByEndPoints/${id}`);
}

// export const addUser = async (user) => {
//     return await axios.post(`${usersUrl}/add`, user);
// }

// export const deleteUser = async (id) => {
//     return await axios.delete(`${usersUrl}/${id}`);
// }

export const editUser = async (id, user) => {
    return await axios.put(process.env.REACT_APP_BASE_URL+`/api/update/EndPoints/${id}`, user)
}