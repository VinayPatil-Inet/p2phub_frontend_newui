import React, { Component, useState, useEffect } from 'react';
import './css/Form.css'; //Import here your file style
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import axios from 'axios';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { propTypes } from 'react-bootstrap/esm/Image';

const btn = {
    "width": "20%",
    "textAlign": "center",
    "marginLeft": "40%",
}

const col = {
    "margin": "6px"
};

const sheader = {
    "backgroundColor": "blue",
    "color": "#ffffff"
}

const sheader_inner1 = {
    "backgroundColor": "gray",
    "color": "white"
}


let PayerContactForm = () => {

    const [userList, setUserList] = useState([]);

    useEffect(() => {
        getPayerList()
        // axios.get(process.env.REACT_APP_BASE_URL+'/api/GetAllPayercontact').then(res => {
        //     setUserList(res.data.data);
        // });
    }, []);

    var url = process.env.REACT_APP_BASE_URL+'/api/payercontact'
    const [data, setData] = useState({
        name: "",
        email: "",
        subject: "",
        message: ""
    })
    function submit(e) {
        e.preventDefault();
        axios.post(url, {
            name: data.name,
            email: data.email,
            subject: data.subject,
            message: data.message
        })
            .then(res => {
                console.log(res.data)
            })

    }
    function handle(e) {
        const newData = { ...data }
        newData[e.target.id] = e.target.value
        setData(newData)
        console.log(newData)

    }
    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);

    function getPayerList(){
        axios.get(process.env.REACT_APP_BASE_URL+'/api/GetAllPayercontact').then(res => {
                setUserList(res.data.data);
            });
        }

    function deletePayer(id) {
        fetch(process.env.REACT_APP_BASE_URL+`/api/payercontact/${id}`, {
            method: 'delete'

        }).then((result) => {
            result.json().then((resp) => {
                console.log(resp)
                getPayerList()
               
            })
        })

    }

    // function Update(id){
    //     console.log(id)
      
    //   //  history.push(''+id)

    // }

    function Update(id) {
        axios
          .put(process.env.REACT_APP_BASE_URL+`/api/GetAllPayercontact/${id}`, {
       
            name:'',
            email:'',
            subject:'', message:'', inserted_by:''
          })
          .then((response) => {
            getPayerList()
            console.log(response,"GetAllPayercontact")
          });
         
      }
 
    

    return (
        <div className="container-fluid p-3">
            <table class="table table-sm mt-3">
                <thead class="thead-dark">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Operation</th>
                  
                </thead>
                <tbody>
                    { userList.map(x => <tr>
                        <td>{x.name}</td>
                        <td>{x.email}</td>
                        <td>{x.subject}</td>
                        <td>{x.message}</td>
                        <td>  <button
                            onClick={() => Update(x.id)} className ="btn btn-success">
                            Edit
                        </button>
                        </td>
                        <td>  <button 
                            onClick={() => deletePayer(x.id)} className ="btn btn-danger">
                            Delete
                        </button>
                        </td>

                    </tr>)}
                    {userList.length == 0 && <tr>
                        <td className="text-center" colSpan="4">
                            <b>No data found to display.</b>
                        </td>
                    </tr>}
                </tbody>
            </table>

        </div>
    );
 



}


export default PayerContactForm;

//https://www.youtube.com/watch?v=9KaMsGSxGno
//https://www.youtube.com/watch?v=x9UEDRbLhJE